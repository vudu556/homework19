﻿

#include <iostream>

class Animal
{
private:

public:
    virtual void Voise() 
    {
        std::cout << "";
    }
};
class Dog : public Animal
{
private:
    
public:
   
    void Voise() override
    {
        std::cout << "Woof!"<<'\n';
    }
};
class Cat : public Animal
{
private:

public:
    void Voise() override
    {
        std::cout << "PRRRRRR!" << '\n';
    }
};
class Cow : public Animal
{
private:

public:
    
        void Voise() override
        {
        std::cout << "MOOOO!" << '\n';
        } 
    
};

int main()
{
    Animal* p[3] = {new Dog, new Cat, new Cow};
    for (int i = 0; i < 3; i++) {

        p[i]->Voise();
    }
}

